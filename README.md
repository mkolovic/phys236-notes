# Computational Physics 1

The lecture notebooks are generated within Docker containers running the
[jupyter/datascience-notebook](https://hub.docker.com/r/jupyter/datascience-notebook/)
image.
